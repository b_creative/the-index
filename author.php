<?php
// Single Author Page
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$current_user = wp_get_current_user();
$context['current_user'] = $current_user;

$templates = ['author.twig'];
Timber::render( $templates, $context );