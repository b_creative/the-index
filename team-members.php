<?php
// Template Name: Team Members Template
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$users = get_users();
$context['users'] = $users;

$templates = ['core-pages/team-members.twig'];

Timber::render( $templates, $context );