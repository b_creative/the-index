<?php
// Global Post Type Controller
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;
the_post();

// attempt to render a corresponding view (twig) file that matches any of the below patterns
// if no matches are found, load the default post view (single.twig)
Timber::render([
	'single-' . $post->ID . '.twig',
	'single-' . $post->post_type . '.twig',
	'single.twig'
], $context);