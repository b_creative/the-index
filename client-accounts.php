<?php
// Template Name: Client Accounts Template
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;
$templates = ['core-pages/client-accounts.twig'];
Timber::render( $templates, $context );