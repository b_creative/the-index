<?php
// Template Name: Clients Template
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;
$templates = ['core-pages/clients.twig'];
Timber::render( $templates, $context );