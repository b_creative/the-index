// Author: Shane Schroll
(function($) {
    $(document).ready(function() {

		// mobile mnenu toggle
		$('#menu-toggle').click(function() {
			$('.x-bar').toggleClass('x-bar-active');
		});

		$(function switchViews() {
			// switch to row view button
			$('#layout-row-btn').click(function() {
				$(this).addClass('active');
				$('#layout-grid-btn').removeClass('active');
				$('.row-layout').fadeIn(500);
				$('.grid-layout').fadeOut(0);
			});

			// switch to grid view button
			$('#layout-grid-btn').click(function() {
				$(this).addClass('active');
				$('#layout-row-btn').removeClass('active');
				$('.row-layout').fadeOut(0);
				$('.grid-layout').fadeIn(500);
			});
		});

		// dropdown credentials list
		$(function credentialReveal() {
			$('.credentials-dropdown').each(function() {
				$(this).hide();
			});

			$('.key-icon').click(function() {
				$(this).toggleClass('key-active');
				$(this).parent().parent().next('.credentials-dropdown').slideToggle(350);
			});
		});

		// nav menu and container left slide on click, leaving just icons as nav items
		// this effect should persist until resized
		$(function menuShift() {
			var nav = $('.nav-container');
			var collapse = $('#collapse-menu');

			collapse.click(function() {
				nav.toggleClass('nav-collapsed');
				$('.copyright p').fadeToggle(100);
				$('.site-wrap').toggleClass('left-shift');
				$('.current-user-profile p').toggleClass('center-align');
				$('.current-user-profile img').toggleClass('image-resize');
			});
		});
	}); // .end Document.Ready
})(jQuery);