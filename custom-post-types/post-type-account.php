<?php
$labels = [
	'name'               => __( 'Accounts', 'mod' ),
	'singular_name'      => __( 'Account', 'mod' ),
	'add_new'            => _x( 'Add Account', 'mod', 'mod' ),
	'add_new_item'       => __( 'Add Account', 'mod' ),
	'edit_item'          => __( 'Edit Account', 'mod' ),
	'new_item'           => __( 'New Account', 'mod' ),
	'view_item'          => __( 'View Account', 'mod' ),
	'search_items'       => __( 'Search Accounts', 'mod' ),
	'not_found'          => __( 'No Accounts found', 'mod' ),
	'not_found_in_trash' => __( 'No Accounts found in Trash', 'mod' ),
	'parent_item_colon'  => __( 'Parent Account:', 'mod' ),
	'menu_name'          => __( 'Accounts', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-groups',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title' ],
];
register_post_type( 'account', $args );