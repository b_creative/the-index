<?php
$labels = [
	'name'               => __( 'Companies', 'mod' ),
	'singular_name'      => __( 'Company', 'mod' ),
	'add_new'            => _x( 'Add Company', 'mod', 'mod' ),
	'add_new_item'       => __( 'Add Company', 'mod' ),
	'edit_item'          => __( 'Edit Company', 'mod' ),
	'new_item'           => __( 'New Company', 'mod' ),
	'view_item'          => __( 'View Company', 'mod' ),
	'search_items'       => __( 'Search Companies', 'mod' ),
	'not_found'          => __( 'No Companies found', 'mod' ),
	'not_found_in_trash' => __( 'No Companies found in Trash', 'mod' ),
	'parent_item_colon'  => __( 'Parent Company:', 'mod' ),
	'menu_name'          => __( 'Companies', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-admin-multisite',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title' ],
];
register_post_type( 'company', $args );