<?php
$labels = [
	'name'               => __( 'Servers', 'mod' ),
	'singular_name'      => __( 'Server', 'mod' ),
	'add_new'            => _x( 'Add Server', 'mod', 'mod' ),
	'add_new_item'       => __( 'Add Server', 'mod' ),
	'edit_item'          => __( 'Edit Server', 'mod' ),
	'new_item'           => __( 'New Server', 'mod' ),
	'view_item'          => __( 'View Server', 'mod' ),
	'search_items'       => __( 'Search Servers', 'mod' ),
	'not_found'          => __( 'No Servers found', 'mod' ),
	'not_found_in_trash' => __( 'No Servers found in Trash', 'mod' ),
	'parent_item_colon'  => __( 'Parent Server:', 'mod' ),
	'menu_name'          => __( 'Servers', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-database-view',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title' ],
];
register_post_type( 'server', $args );