<?php
$labels = [
	'name'               => __( 'BC Plugins', 'mod' ),
	'singular_name'      => __( 'BC Plugin', 'mod' ),
	'add_new'            => _x( 'Add BC Plugin', 'mod', 'mod' ),
	'add_new_item'       => __( 'Add BC Plugin', 'mod' ),
	'edit_item'          => __( 'Edit BC Plugin', 'mod' ),
	'new_item'           => __( 'New BC Plugin', 'mod' ),
	'view_item'          => __( 'View BC Plugin', 'mod' ),
	'search_items'       => __( 'Search BC Plugins', 'mod' ),
	'not_found'          => __( 'No BC Plugins found', 'mod' ),
	'not_found_in_trash' => __( 'No BC Plugins found in Trash', 'mod' ),
	'parent_item_colon'  => __( 'Parent BC Plugin:', 'mod' ),
	'menu_name'          => __( 'BC Plugins', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => false,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-block-default',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title' ],
];
register_post_type( 'bcplugin', $args );