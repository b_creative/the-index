<?php
$labels = [
	'name'               => __( 'Clients', 'mod' ),
	'singular_name'      => __( 'Client', 'mod' ),
	'add_new'            => _x( 'Add Client', 'mod', 'mod' ),
	'add_new_item'       => __( 'Add Client', 'mod' ),
	'edit_item'          => __( 'Edit Client', 'mod' ),
	'new_item'           => __( 'New Client', 'mod' ),
	'view_item'          => __( 'View Client', 'mod' ),
	'search_items'       => __( 'Search Clients', 'mod' ),
	'not_found'          => __( 'No Clients found', 'mod' ),
	'not_found_in_trash' => __( 'No Clients found in Trash', 'mod' ),
	'parent_item_colon'  => __( 'Parent Client:', 'mod' ),
	'menu_name'          => __( 'Clients', 'mod' ),
];

$args = [
	'labels'              => $labels,
	'hierarchical'        => false,
	'description'         => '',
	'taxonomies'          => [],
	'public'              => true,
	'show_ui'             => true,
	'show_in_menu'        => true,
	'show_in_admin_bar'   => true,
	'show_in_rest'		  => false,
	'menu_icon'           => 'dashicons-businessperson',
	'show_in_nav_menus'   => false,
	'publicly_queryable'  => true,
	'exclude_from_search' => false,
	'has_archive'         => false,
	'query_var'           => true,
	'can_export'          => true,
	'rewrite'             => true,
	'capability_type'     => 'post',
	'supports'            => [ 'title' ],
];
register_post_type( 'client', $args );