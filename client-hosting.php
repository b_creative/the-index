<?php
// Template Name: Client Hosting Template
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;
$templates = ['core-pages/client-hosting.twig'];
Timber::render( $templates, $context );