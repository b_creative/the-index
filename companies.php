<?php
// Template Name: Companies Template
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;
$templates = ['core-pages/companies.twig'];
Timber::render( $templates, $context );