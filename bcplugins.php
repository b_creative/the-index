<?php
// Template Name: BC Plugins Template
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// get our posts with pagination
$plugins = Timber::get_posts([
	'post_type' => 'bcplugin',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
]);

$context['plugins'] = $plugins;

$templates = ['core-pages/bcplugins.twig'];
Timber::render( $templates, $context );