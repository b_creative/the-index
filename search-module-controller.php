<?php
// Core search module functions and filter setups
$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

$templates = ['search-module.twig'];
Timber::render( $templates, $context );
