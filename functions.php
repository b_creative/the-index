<?php

// change 'views' directory to 'templates'
Timber::$locations = __DIR__ . '/templates';

class MODSite extends TimberSite {

	/**
	 * To add items to the site class ($this)
	 * 
	 * First add an action/filter hook to the constructor plus your custom function name
	 * then create your function outside of the constructor, but still inside this site class
	 * The action hooks in the constructor will ALWAYS run first, then filters (regardless of order)

	 * To add global actions or filters, put both the hook and function outside of this site class
	 * https://timber.github.io/docs/reference/timber-post/#__construct
	*/

	function __construct() {
		// Action Hooks //
		add_action( 'after_setup_theme', [ $this, 'after_setup_theme' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_scripts' ] );
		add_action( 'admin_head', [ $this, 'admin_head_css' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu_cleanup'] );
		add_action( 'init', [ $this, 'register_post_types' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'style_login' ] );

		// Filter Hooks //
		add_filter( 'timber_context', [ $this, 'add_to_context' ] );
		add_filter( 'manage_pages_columns', [ $this, 'remove_pages_count_columns' ] );

		// allows use of $this
		parent::__construct();
	}

	// hide nags and unused items
	function admin_head_css() {
		?>
		<style type="text/css">
			.update-nag,
			#wp-admin-bar-comments,
			#wp-admin-bar-updates,
			#wp-admin-bar-new-content
			#comments, .column-comments,
			#wp-admin-bar-new-content { display: none !important; }

			#menu-posts-client { margin-top: 1.5rem !important; }
			#menu-posts-bcplugin { margin-bottom: 1rem !important; }
		</style>
		<?php
	}

	// WP admin login styles
	function style_login() {
		?>
		<style type="text/css">
			#login h1, .login h1 {
				background-color: transparent;
				padding: 0;
				border:unset;
			}

			#login h1 a, .login h1 a {
				background-image: url('<?= get_stylesheet_directory_uri() . '/assets/media/logo.png' ?>') !important;
				background-position: center;
				width: 100%;
				height: 8rem;
				background-size: cover;
			}

			#backtoblog { display: none !important; }
		</style>
		<?php
	}

	// enqueue styles & scripts
	function enqueue_scripts() {
		$version = filemtime( get_stylesheet_directory() . '/style.css' );
		wp_enqueue_style( 'core-css', get_stylesheet_directory_uri() . '/style.css', [], $version );
		wp_enqueue_script( 'mod-js', get_template_directory_uri() . '/assets/js/site-dist.js', ['jquery'], $version );
	}

	// Custom context helper functions (callable)
	// https://timber.github.io/docs/guides/cookbook-twig/
	function add_to_context( $context ) {
		$context['site']           = $this; // the current site class
		$context['date_year']      = date('Y');
		$context['options']        = get_fields( 'option' );
		$context['is_front_page']  = is_front_page();
		$context['is_admin']	   = current_user_can('administrator');

		return $context;
	}

	// -Menus / -Theme Support / -ACF Options Page
	function after_setup_theme() {
		register_nav_menu( 'primary', 'Primary Navigation' );
		add_theme_support( 'menus' );
		add_theme_support( 'align-wide' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'disable-custom-colors' ); // disable color picker wheel
	}

	// include post types
	function register_post_types() {
		include_once('custom-post-types/post-type-client.php');
		include_once('custom-post-types/post-type-account.php');
		include_once('custom-post-types/post-type-company.php');
		include_once('custom-post-types/post-type-server.php');
		include_once('custom-post-types/post-type-bcplugin.php');
	}

	// remove unused items from admin menu
	function admin_menu_cleanup() {
		$user = wp_get_current_user();

		if( $user && isset($user->user_login) && ('shane' || 'ben') != $user->user_login ) {
			remove_menu_page( 'wppusher' ); // prevent tampering with repo setup
		}

		remove_menu_page( 'edit.php' ); // Default Posts
		remove_menu_page( 'edit-comments.php' ); // Comments
		remove_menu_page( 'tools.php' ); // tools menu
	}

	// removed comment column from posts pages
	function remove_pages_count_columns( $defaults ) {
		unset($defaults['comments']);
		return $defaults;
	}
} // End of MODSite class

new MODSite();

// main site nav - used to render the top left and bottom main menus
function mod_render_primary_menu() {
	wp_nav_menu([
		'theme_location' => 'primary',
		'container'      => false,
		'menu_id'        => 'primary-menu',
	]);
}

// redirect users who are not admins to the home page, admin bar disabled
function custom_login_redirect( $redirect_to, $request, $user ) {
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if( in_array( 'administrator', $user->roles ) ) {
			$redirect_to = admin_url();
		} else if ( in_array( 'subscriber', $user->roles ) ) {
			$redirect_to = home_url( '/' );
		} else {
			$redirect_to = home_url();
		}
	}
	return $redirect_to;
}
add_filter( 'login_redirect', 'custom_login_redirect', 10, 3 );